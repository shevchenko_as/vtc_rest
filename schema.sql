-- db: rest
-- user: rest
-- passwd: rest

-- REATE DATABASE rest
--   WITH OWNER = rest
--        ENCODING = 'UTF8'
--        TABLESPACE = pg_default
--        LC_COLLATE = 'Russian_Russia.1251'
--        LC_CTYPE = 'Russian_Russia.1251'
--        CONNECTION LIMIT = -1;

create table if not exists rest_user (
    id serial not null primary key,
    user_login varchar(50) not null unique,
    login_type boolean not null,
    passwd_hash varchar(64) not null
);

create table if not exists rest_token (
    user_id integer not null REFERENCES rest_user (id) on delete cascade on update cascade,
    token varchar(32) not null primary key,
    token_expire timestamp with time zone
);

create index token_token_expire on rest_token (
    token_expire desc
);
