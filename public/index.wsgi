import sys
import os

parent_dir = os.path.join(os.path.dirname(__file__), '..')
parent_dir = os.path.realpath(parent_dir)
sys.path.insert(0, parent_dir)
from vtc_rest import app as application
