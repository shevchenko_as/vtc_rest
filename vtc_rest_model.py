from sqlalchemy import Column, Integer, String, Boolean, DateTime, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from datetime import datetime, timedelta, timezone
from guid import GUID
import re

from vtc_rest_helper import sha256


REGEX_PHONE = '^[0-9]{11,13}$'
REGEX_EMAIL = '^[-_0-9A-Za-z]+@([-_0-9A-Za-z]+\.)+[A-Za-z]{2,}$'

Base = declarative_base()


class User(Base):
    __tablename__ = 'rest_user'
    id = Column(Integer, primary_key=True, nullable=False)
    user_login = Column(String(50), unique=True, nullable=False)
    login_type = Column(Boolean, default=True, nullable=False)
    passwd_hash = Column(String(64), nullable=False)

    def __init__(self, user_login, pure_password):
        self.user_login = user_login
        self.login_type = re.match(REGEX_PHONE, user_login) is not None
        self.passwd_hash = sha256(pure_password)

    def __repr__(self):
        if self.id is not None:
            return "<User('{id}, {user_login}')>".format(id=self.id, user_login=self.user_login)
        else:
            return "<User({user_login})>".format(user_login=self.user_login)


class Token(Base):
    __tablename__ = 'rest_token'
    user_id = Column(Integer, ForeignKey('rest_user.id'), nullable=False)
    token = Column(String(32), primary_key=True)
    token_expire = Column(DateTime(timezone=True))

    def __init__(self, user_id):
        self.user_id = user_id
        self.token = GUID().uuid.replace('-', '')
        self.token_expire = Token.getExpirationTime()
        
    def __repr__(self):
        return "<Token({user_id}, {token}, {expire})>".format(user_id=self.user_id, token=self.token, expire=self.token_expire.strftime('%Y-%m-%d %H:%M:%S.%f%z'))

    @staticmethod
    def getExpirationTime():
        return datetime.now(tz=timezone.utc) + timedelta(minutes=10)

    def setExpirationTime(self):
        self.token_expire = Token.getExpirationTime()
