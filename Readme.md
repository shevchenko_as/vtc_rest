## Description

This is a training task for learning python, Flask, SQLAlchemy.

REST API with bearer token authorization. Release CORS. Use PostgreSQL. Generate token on /signin request, valid through 10 minutes. Prolongate current token validity with any request but /signin, /signup. 

Version 1.1.
 
## Installation

1. Install PostgreSQL
2. `sudo -u postgres createuser -d -l -P rest`  
   password for rest: rest
3. `sudo -u postgres createdb -E UTF8 -l ru_RU.UTF-8 -O rest rest`
4. `psql -U rest -W -f schema.sql rest`  
   password for rest: rest
5. Use python 3.*
6. Install python modules (if not installed):
    * Flask
    * SQLAlchemy
    * psycopg2
    * pycurl
    * guid
7. With Apache 2.4: create virtual host with some configuration such as:  
    <VirtualHost *:8880>  
        ServerAdmin webmaster@localhost  
        WSGIScriptAlias / /full-path-to-this-directory/public/index.wsgi  
        WSGIPassAuthorization On  
        <Directory /full-path-to-this-directory/public/ >  
            AllowOverride All  
            Options Indexes FollowSymLinks  
            Require all granted  
            WSGIScriptReloading On  
        </Directory\>    
        LogLevel notice  
        ErrorLog ${APACHE\_LOG\_DIR}/wsgi_error.log  
        CustomLog ${APACHE\_LOG\_DIR}/wsgi\_access.log combined  
    </VirtualHost>
8. Or use Flask development server by calling  
   `python vtc_rest.py`

***

## API

### Common notes
Any response with JSON body contain CORS header:  
`Access-Control-Allow-Origin: *`

### /signup
Create user profile marking login type as "phone" or "mail".

#### Request
* method: **POST**
* data format: **JSON** object
* examples:  
    `{"id": "79101234567", "password": "qwerty"}`  
    `{"id": "user@mail.net", "password": "asdf"}`  
**id** and **password** properties are mandatory. **id** is a phone number or an email.

#### Response
* If input is not a valid JSON or some mandatory property not present then response `400 Bad request` else response `200 OK`
* Content-type: **application/json**
* Application error example:  
    `{
        "error": {
            "code": 10,
            "message": "login already registred, use another email or phone number"
        }
    }`
* Successful response example:  
`{"token": "5447a561bbb54e5f8e627de26b3423e0"}`  


### /signin
Get new token.

#### Request
Same as for **/signup**

#### Response
* Application error examples:  
  `{"error": {"code": 20, "message": "user not found"}}`  
  `{"error": {"code": 21, "message": "password not valid"}}`
* Successful response same as for **/signup**


### /info
Get user id and id type: "phone" or "mail".  
Prolongate current token validity.

#### Request
* method: **GET**
* mandatory header:  
  `Authorization: token-received-from-signin`

#### Response
* If header "Authorization" is not present or not contain valid token, `401 Unauthorized` responsed. 
* Content-type: **application/json**
* Successful response examples:  
  `{"id": "79101234567", "type": "phone"}`  
  `{"id": "user@mail.net", "type": "mail"}`

### /latency
Return latency of HTTP HEAD request to google.com site in microseconds.  
Prolongate current token validity.

#### Request
* method: **GET**
* mandatory header:  
  `Authorization: token-received-from-signin`

#### Response
* If header "Authorization" is not present or not contain valid token, `401 Unauthorized` responsed. 
* Content-type: **application/json**
* Application error example:  
  `{"error": {"code": 40, "message": "Latency measurement error: (7, Couldn't connect to host)"}}`
* Successful response example:  
  `{"latency": 312500}`  


### /logout
May use **all** GET-parameter: `/logout?all=true`

If all parameter set to any but true or not present, delete only current token (used for this request authorization).

If `all=true` delete all tokens of user ownes current token.

Return deleted tokens count.

#### Request
* method: **GET**
* mandatory header:  
  `Authorization: token-received-from-signin`

#### Response
* If header "Authorization" is not present or not contain valid token, `401 Unauthorized` responsed. 
* Content-type: **application/json**
* Successful response examples:  
  `{"deleted": 2}`  

***

## TODO
Use JWT tokens instead of plain string.


## What's new?
* Version 1.1: Standard header "Authorization" used instead of custom "X-Authorization". Apache2 + mod_wsgi configured by using "WSGIPassAuthorization On" option.
