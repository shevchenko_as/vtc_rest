from flask import Flask, request, \
     session, redirect, url_for
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from datetime import datetime, timezone
import json
import re

from vtc_rest_model import User, Token, REGEX_PHONE, REGEX_EMAIL
from vtc_rest_helper import sha256, hostHEADLatencyInMcs


app = Flask(__name__)
app.config.from_pyfile('./vtc_rest.conf')
app.debug = (__name__ == '__main__')
database_uri = 'postgresql://' + \
               app.config['DB_LOGIN'] + ':' + \
               app.config['DB_PASSWD'] + '@' + \
               app.config['DB_HOST'] + ':' + \
               str(app.config['DB_PORT']) + '/' + \
               app.config['DB_NAME']
engine = create_engine(database_uri, echo=app.debug)
Session = sessionmaker(bind=engine)
session = Session()


@app.route('/')
def root():
    return redirect(url_for('info'), 301)


@app.route('/signup', methods=['POST'])
def signup():
    data_uploaded = request.get_json(force=True)
    login = data_uploaded.get('id')
    passwd = data_uploaded.get('password')
    if login is None or passwd is None:
        return 'Bad request: id or password not present', 400

    if re.match(REGEX_PHONE, login) is None and \
       re.match(REGEX_EMAIL, login) is None:
        return makeErrorResponse(
            code=10,
            message='user login must be a phone number or an email'
        )

    user = session.query(User).filter_by(user_login=login).first()
    if user is not None:
        return makeErrorResponse(
            code=11,
            message='login already registred, use another email or phone number'
        )

    user = User(login, passwd)
    session.add(user)
    session.commit() # oh, else we do not receive rest_user.id

    token = Token(user.id)
    # read token string before commit to avoid one more 'select' query to DB
    result = {'token': token.token}
    session.add(token)
    session.commit()

    return makeResponse(result)


@app.route('/signin', methods=['POST'])
def signin():
    data_uploaded = request.get_json(force=True)
    login = data_uploaded.get('id')
    passwd = data_uploaded.get('password')
    if login is None or passwd is None:
        return 'Bad request: id or password not present', 400

    user = session.query(User).filter_by(user_login=login).first()
    if user is None:
        return makeErrorResponse(code=20, message='user not found')
    
    if user.passwd_hash != sha256(passwd):
        return makeErrorResponse(code=21, message='password not valid')

    token = Token(user.id)
    # read token string before commit to avoid one more 'select' query to DB
    result = {'token': token.token}
    session.add(token)
    session.commit()

    return makeResponse(result)


@app.route('/info', methods=['GET'])
def info():
    token_str, error = getTokenFromRequest()
    if error is not None:
        return error

    user = session.query(Token.user_id, User.login_type).\
                   join(User, User.id == Token.user_id).\
                   filter(Token.token == token_str).\
                   filter(Token.token_expire > datetime.now(tz=timezone.utc)).\
                   first()
    if user is None:
        return 'Unauthorized', 401

    result = {
        'id': user[0],
        'type': 'phone' if user[1] else 'mail'
    }
    session.query(Token).\
            filter(Token.token == token_str).\
            update({Token.token_expire: Token.getExpirationTime()}, synchronize_session=False)
    session.commit()

    return makeResponse(result)


@app.route('/latency', methods=['GET'])
def latency():
    token, error = getToken()
    if error is not None:
        return error

    error = None
    try:
        latency = hostHEADLatencyInMcs('google.com')
    except Exception as err:
        error = str(err)

    token.setExpirationTime()
    session.commit()

    if error is not None:
        return makeErrorResponse(40, 'Latency measurement error: ' + error)
    return makeResponse({'latency': latency})


@app.route('/logout', methods=['GET'])
def logout():
    token, error = getToken()
    if error is not None:
        return error

    clear_all_tokens = (request.args.get('all', 'false').lower() == 'true')
    if clear_all_tokens:
        deleted = session.query(Token).\
                          filter(Token.user_id == token.user_id).\
                          delete(synchronize_session=False)
    else:
        session.delete(token)
        deleted = 1
    session.commit()

    return makeResponse({'deleted': deleted})


def getToken():
    token_str, error = getTokenFromRequest()
    if error is not None:
        return None, error

    token = session.query(Token).\
                    filter(Token.token == token_str).\
                    filter(Token.token_expire > datetime.now(tz=timezone.utc)).\
                    first()
    if token is None:
        return None, ('Unauthorized', 401)
    else:
        return token, None


def getTokenFromRequest():
    token_str = request.headers.get('Authorization')
    if token_str is None:
        return None, ('Unauthorized', 401)
    else:
        return token_str, None


def makeErrorResponse(code, message):
    result = {"error": {
        "code": code,
        "message": message
    }}
    return makeResponse(result)


def makeResponse(values_as_dict):
    return (
        json.dumps(values_as_dict, sort_keys=True),
        200,
        {
            'Content-type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        }
    )


if __name__ == '__main__':
    app.run()
