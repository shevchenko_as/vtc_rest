import hashlib
import pycurl
import time


def sha256(input_str):
    h = hashlib.sha256()
    h.update(bytes(input_str, encoding='utf-8'))
    return h.hexdigest()


def hostHEADLatencyInMcs(uri):
    curl = pycurl.Curl()
    curl.setopt(curl.URL, uri)
    curl.setopt(curl.NOBODY, True) # this line force HEAD instead of GET
    time_from = time.time()
    curl.perform()
    latency = int((time.time() - time_from) * 1000000)
    curl.close()

    return latency
